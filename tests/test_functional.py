# -*- coding: utf-8 -*-
# This program is part of GASP, a toolkit for newbie Python Programmers.
# Copyright (C) 2009, the GASP Development Team
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Functional Tests"""

import unittest

import gasp


class GaspFunctionalTests(unittest.TestCase):

    def tearDown(self):
        try:
            gasp.end_graphics()
        except SystemExit:
            pass

    def test_open_window(self):
        gasp.begin_graphics()

    def test_Line(self):
        gasp.begin_graphics()
        gasp.Line((200, 100), (100, 300))



